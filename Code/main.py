from ensurepip import version
from BlockChain import *
from OrthoTypes import EOXFile

def create_aaos_block():
    aaosBlock = OrthoBlock()
    aaosBlock.implant.anatomy = "knee"
    aaosBlock.implant.vendor = "ZB"
    aaosBlock.implant.uid = "123456789"

    aaosBlock.implant.surgeon.fName = "Sfirst"
    aaosBlock.implant.surgeon.lName = "SLirst"
    aaosBlock.implant.surgeon.hospital.name = "Name Hospital"
    aaosBlock.implant.surgeon.speciality = "Knee:Hips"
    aaosBlock.implant.surgeon.hospital.location = "Location"
    aaosBlock.implant.surgeon.hospital.speciality = "Knee:Hips"
    aaosBlock.implant.surgeon.rehabilitation.name = "Knee:Hips"
    aaosBlock.implant.surgeon.rehabilitation.type = "Location"
    
    aaosBlock.implant.patient.fName = "first"
    aaosBlock.implant.patient.lName = "last"
    aaosBlock.implant.patient.age = "30"
    aaosBlock.implant.patient.sex = "m"
    aaosBlock.implant.patient.patientMedicalRecord.frequency = "once"
    aaosBlock.implant.patient.patientMedicalRecord.medication = "pain medication"
    aaosBlock.implant.patient.wearableActivitiesRecord.name = "pedo-meter"
    aaosBlock.implant.patient.wearableActivitiesRecord.type = "steps"

    aaosBlock.implant.manufacturing.name = "Zimmer Biomet"
    aaosBlock.implant.manufacturing.productLiterature.name = "Knee Manual"
    aaosBlock.implant.manufacturing.productLiterature.type = "e-books"
    aaosBlock.implant.manufacturing.competetiveProduct.name = "Mako"
    aaosBlock.implant.manufacturing.competetiveProduct.vendor = "Stryker"

    aaosBlock.implant.adverseEvents.name = "Revision"
    aaosBlock.implant.adverseEvents.governingBody = "FDA"
    aaosBlock.implant.adverseEvents.type = "Zimmer Biomet"
    aaosBlock.implant.adverseEvents.associatedProducts.name = "Knee"
    aaosBlock.implant.adverseEvents.associatedProducts.manufacturer = "JnJ"
    aaosBlock.implant.adverseEvents.pricing.price = "Mako"
    aaosBlock.implant.adverseEvents.pricing.type = "dollars"

    aaosBlock.implant.eoxFile.filePath = "C:\path\path"
    aaosBlock.implant.eoxFile.version = "1.0"
    aaosBlock.implant.adverseEvents.Name = "Failure 1"
    return aaosBlock

aaosBlockChain = Blockchain()
aaosBlockChain.update_AAOSblock(create_aaos_block())
print("Displaying the chain")
aaosBlockChain.display_chain()
print("look for ....")
aaosBlockChain.find_BlockBy("first")
#aaosBlockChain.create_AAOSblock_from_transaction([t3, t4])
#aaosBlockChain.create_AAOSblock_from_transaction([t5, t6])


