import json
from json import JSONEncoder
from sched import scheduler

class Implant:
    def __init__(self) -> None:
        self.anatomy = ""
        self.version = ""
        self.uid = ""
        self.surgeon = Surgeon()
        self.patient = Patient()
        self.manufacturing = Manufacturing()
        self.adverseEvents = AdverseEvents()
        self.eoxFile = EOXFile()

class ImplantEncoder(JSONEncoder):
        def default(self, o):
            return o.__dict__

#Surgeon
class Surgeon:
    def __init__(self) -> None:
        self.fName = ""
        self.lName = ""
        self.speciality = ""
        self.hospital = Hospital()
        self.rehabilitation = Rehabilitation()

class SurgeonEncoder(JSONEncoder):
        def default(self, o):
            return o.__dict__
        
class Hospital:
    def __init__(self) -> None:
        self.name = ""
        self.location = ""
        self.speciality = ""

class HospitalEncoder(JSONEncoder):
        def default(self, o):
            return o.__dict__

class Rehabilitation:
    def __init__(self) -> None:
        self.name = ""
        self.type = ""

class RehabilitationEncoder(JSONEncoder):
        def default(self, o):
            return o.__dict__


#Patient
class Patient:
    def __init__(self) -> None:
        self.fName = ""
        self.lName = ""
        self.age = ""
        self.sex = ""
        self.patientMedicalRecord = PatientMedicalRecord()
        self.patientSchedulingInsurance = PatientSchedulingInsuranceRecord()
        self.wearableActivitiesRecord = WearableActivitiesRecord()

class PatientEncoder(JSONEncoder):
        def default(self, o):
            return o.__dict__


class PatientMedicalRecord:
    def __init__(self) -> None:
        self.medication = ""
        self.frequency = ""

class PatientMedicalRecordEncoder(JSONEncoder):
        def default(self, o):
            return o.__dict__


class PatientSchedulingInsuranceRecord:
    def __init__(self) -> None:
        self.insuranceName = ""
        self.insuranceType = ""
        
class PatientSchedulingInsuranceRecordEncoder(JSONEncoder):
        def default(self, o):
            return o.__dict__

class WearableActivitiesRecord:
    def __init__(self) -> None:
        self.name = ""
        self.type = ""

class WearableRecordEncoder(JSONEncoder):
        def default(self, o):
            return o.__dict__


#Manufacturing
class Manufacturing:
    def __init__(self) -> None:
        self.name = ""
        self.productLiterature = ProductLiterature()
        self.competetiveProduct = CompetetiveProducts()

class ManufacturerEncoder(JSONEncoder):
        def default(self, o):
            return o.__dict__

class ProductLiterature:
    def __init__(self) -> None:
        self.name = ""
        self.type = ""

class ProductLiteratureEncoder(JSONEncoder):
        def default(self, o):
            return o.__dict__

class CompetetiveProducts:
    def __init__(self) -> None:
        self.name = ""
        self.vendor = ""

class CompetetiveProductEncoder(JSONEncoder):
        def default(self, o):
            return o.__dict__

#Adverse events
class AdverseEvents:
    def __init__(self) -> None:
        self.name = ""
        self.type = ""
        self.governingBody = ""
        self.associatedProducts = AssociatedProducts()
        self.pricing = Pricing()

class AdverseEventEncoder(JSONEncoder):
        def default(self, o):
            return o.__dict__

class AssociatedProducts:
    def __init__(self) -> None:
        self.name = ""
        self.manufacturer = Manufacturing()

class AssociatedProductsEncoder(JSONEncoder):
        def default(self, o):
            return o.__dict__

class Pricing:
    def __init__(self) -> None:
        self.type = ""
        self.price = ""

class PricingEncoder(JSONEncoder):
        def default(self, o):
            return o.__dict__



#Block
class EOXFile:
    def __init__(self) -> None:
        self.filePath = ''
        self.version = ''

class EOXFileEncoder(JSONEncoder):
        def default(self, o):
            return o.__dict__

class OrthoBlock:
    def __init__(self) -> None:
        self.implant = Implant()

class OrthoBlockEncoder(JSONEncoder):
        def default(self, o):
            return o.__dict__


#Reference
"""
NFT
https://geekflare.com/finance/nft-creation-and-applications/

Create a template for the base object or open an exisiting model
https://pynative.com/make-python-class-json-serializable/
https://geekflare.com/create-a-blockchain-with-python/#:~:text=Creating%20the%20Block%20class%20Open%20your%20favorite%20code,module%20that%20lets%20us%20create%20one-way%20encrypted%20messages
"""
