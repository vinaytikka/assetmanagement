
import hashlib

class AAOSblock:

    def __init__(self, previous_block_hash, transaction_list):

        self.previous_block_hash = previous_block_hash
        self.transaction_list = transaction_list

        self.block_data = transaction_list
        self.block_hash = hashlib.sha256(str(self.block_data).encode()).hexdigest()

