
from AssestBlock import *
from OrthoTypes import OrthoBlock, OrthoBlockEncoder
import json

class Blockchain:
    def __init__(self):
        self.chain = []
        self.generate_AAOS_genesis_block()

    def generate_AAOS_genesis_block(self):        
        self.chain.append(AAOSblock("0", OrthoBlock()))

    def update_AAOSblock(self, transaction_list):
        previous_block_hash = self.last_block.block_hash
        self.chain.append(AAOSblock(previous_block_hash, transaction_list))

    def find_BlockBy(self,patientDetails):
        match = list(filter(lambda x: patientDetails in x.block_data.implant.patient.fName, self.chain))  
        if(len(match) > 0):
            print("found match for -"+ patientDetails +"- count : " +str(len(match)))
            for i in range(len(match)):
                dataStr = json.dumps(match[i].block_data, indent=4, cls=OrthoBlockEncoder)
                print(f"Data {i + 1}: {dataStr}")
                print(f"Hash {i + 1}: {match[i].block_hash}\n")
        else:
            print("found match for -"+ patientDetails +"- count : " + str(len(match)))

    def display_chain(self):
        for i in range(len(self.chain)):
            dataStr = json.dumps(self.chain[i].block_data, indent=4, cls=OrthoBlockEncoder)
            print(f"Data {i + 1}: {dataStr}")
            print(f"Hash {i + 1}: {self.chain[i].block_hash}\n")

    @property
    def last_block(self):
        return self.chain[-1]


    